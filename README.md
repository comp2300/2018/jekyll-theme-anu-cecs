# jekyll-theme-anu-cecs

## Installation

Add this line to your projects `Gemfile` to always use the latest release:

```ruby
gem "jekyll-theme-anu-cecs", :git => "https://gitlab.cecs.anu.edu.au/cecstlweb/jekyll-theme-anu-cecs.git"
```
or to specify a specific version using tags

```ruby
gem "jekyll-theme-anu-cecs", :git => "https://gitlab.cecs.anu.edu.au/cecstlweb/jekyll-theme-anu-cecs.git", :tag => '1.0.11'
```

A list of avaiable tags can be viewed in gitlab [here](https://gitlab.cecs.anu.edu.au/cecstlweb/jekyll-theme-anu-cecs/tags)

And add this line to your projects site's `_config.yml`:

```yaml
theme: jekyll-theme-anu-cecs
```

And then execute:

    $ bundle install

## Contributing

Please submit Issues and Merge Requests are welcome through GitLab at https://gitlab.cecs.anu.edu.au/cecstlweb/jekyll-theme-anu-cecs.
